// irled.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

int width = 640;
int heigth = 480;
cv::Point2f center;
std::vector<glm::vec3> pos, rot;

double slope(const std::vector<double>& x, const std::vector<double>& y) 
{
    const auto n = x.size();
    const auto s_x = std::accumulate(x.begin(), x.end(), 0.0);
    const auto s_y = std::accumulate(y.begin(), y.end(), 0.0);
    const auto s_xx = std::inner_product(x.begin(), x.end(), x.begin(), 0.0);
    const auto s_xy = std::inner_product(x.begin(), x.end(), y.begin(), 0.0);
    const auto a = (n * s_xy - s_x * s_y) / (n * s_xx - s_x * s_x);
    return a;
}

void drawCube()
{
    glBegin(GL_QUADS);
    glColor3f(0.0f, 1.0f, 0.0f);
    glVertex3f(1.0f, 1.0f, -1.0f);
    glVertex3f(-1.0f, 1.0f, -1.0f);
    glVertex3f(-1.0f, 1.0f, 1.0f);
    glVertex3f(1.0f, 1.0f, 1.0f);

    glColor3f(1.0f, 0.5f, 0.0f);
    glVertex3f(1.0f, -1.0f, 1.0f);
    glVertex3f(-1.0f, -1.0f, 1.0f);
    glVertex3f(-1.0f, -1.0f, -1.0f);
    glVertex3f(1.0f, -1.0f, -1.0f);

    glColor3f(1.0f, 0.0f, 0.0f);
    glVertex3f(1.0f, 1.0f, 1.0f);
    glVertex3f(-1.0f, 1.0f, 1.0f);
    glVertex3f(-1.0f, -1.0f, 1.0f);
    glVertex3f(1.0f, -1.0f, 1.0f);

    glColor3f(1.0f, 1.0f, 0.0f);
    glVertex3f(1.0f, -1.0f, -1.0f);
    glVertex3f(-1.0f, -1.0f, -1.0f);
    glVertex3f(-1.0f, 1.0f, -1.0f);
    glVertex3f(1.0f, 1.0f, -1.0f);

    glColor3f(0.0f, 0.0f, 1.0f);
    glVertex3f(-1.0f, 1.0f, 1.0f);
    glVertex3f(-1.0f, 1.0f, -1.0f);
    glVertex3f(-1.0f, -1.0f, -1.0f);
    glVertex3f(-1.0f, -1.0f, 1.0f);

    glColor3f(1.0f, 0.0f, 1.0f);
    glVertex3f(1.0f, 1.0f, -1.0f);
    glVertex3f(1.0f, 1.0f, 1.0f);
    glVertex3f(1.0f, -1.0f, 1.0f);
    glVertex3f(1.0f, -1.0f, -1.0f);
    glEnd();
}

void render()
{
    glm::mat4 proj;
    glm::mat4 modelview;
    
    // Go to perspective mode
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    proj = glm::perspective(45.f, (float)width / (float)heigth, .1f, 1000.f);
    glMultMatrixf(glm::value_ptr(proj));
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glViewport(0, 0, width, heigth);
    glClearColor(0.2f, 0.2f, 0.2f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    float mul = 3;
    glTranslatef((-(center.x / 640 * 2) + 1) * mul, ((center.y / 480 * 2) - 1) * mul, 0);
    for (int i = 0; i < pos.size(); i++)
    {
        glPushMatrix();
        glTranslatef(pos[i].x * 10, pos[i].y * 10, -pos[i].z * 10);
        glScalef(0.3, 0.3, 0.3);
        drawCube();
        glPopMatrix();
    }
}

bool collinear(int x1, int y1, int x2, int y2, int x3, int y3) 
{
    return (y1 - y2) * (x1 - x3) == (y1 - y3) * (x1 - x2);
}

int _tmain(int argc, _TCHAR* argv[])
{
    GLFWwindow *win;
    glfwInit();
    win = glfwCreateWindow(width, heigth, "ir head tracking", NULL, NULL);
    glfwMakeContextCurrent(win);
    glEnable(GL_DEPTH_TEST);

    glfwSetWindowSizeCallback(win, [](GLFWwindow* window, int w, int h)
    {
        width = w;
        heigth = h;
    });

    // init
    for (int i = 0; i < 100; i++)
    {
        float x = std::rand() % 1000 * 0.002 - 1;
        float y = std::rand() % 1000 * 0.002 - 1;
        float z = std::rand() % 1000 * 0.001 + 1;
        pos.push_back(glm::vec3(x, y, z));
    }

    std::vector<std::vector<cv::Point>> contours;
    std::vector<cv::KeyPoint> keyPoints;
    std::vector<std::vector<cv::Point>> approxContours;

    cv::Mat frame, threshold, blur, threshold2, canny, channels[3], result;
    cv::VideoCapture cam(1);
    cam.set(CV_CAP_PROP_EXPOSURE, 15);
    cam.set(CV_CAP_PROP_BRIGHTNESS, 0);
    cam.set(CV_CAP_PROP_GAMMA, 100);

    cv::SimpleBlobDetector::Params detectorParams;
    detectorParams.thresholdStep = 20;
    detectorParams.minThreshold = 200;
    detectorParams.maxThreshold = 255;
    detectorParams.minDistBetweenBlobs = 2;

    detectorParams.filterByArea = true;
    detectorParams.minArea = 2;
    detectorParams.maxArea = 300;
    
    detectorParams.filterByConvexity = false;
    detectorParams.minConvexity = 0.3;
    detectorParams.maxConvexity = 10;
    
    detectorParams.filterByInertia = false;
    detectorParams.minInertiaRatio = 0.01;
    detectorParams.maxInertiaRatio = 10;

    detectorParams.filterByColor = false;
    detectorParams.blobColor = 0;

    detectorParams.filterByCircularity = false;
    detectorParams.minCircularity = 0.01;
    detectorParams.maxCircularity = 10;

    cv::SimpleBlobDetector detector(detectorParams);
    detector.create("SimpleBlob");

    while (!glfwWindowShouldClose(win))
    {
        cam >> frame;
        if (frame.rows == 0)
            continue;

        cv::split(frame, channels);
        detector.detect(channels[0], keyPoints);
        
        result = cv::Mat(cv::Size(frame.cols, frame.rows), frame.type());
        cv::drawKeypoints(result, keyPoints, result, CV_RGB(255, 0, 0), cv::DrawMatchesFlags::DEFAULT);

        center = cv::Point2f();
        for (cv::KeyPoint p : keyPoints)
        {
            center += p.pt;
        }
        center.x /= keyPoints.size();
        center.y /= keyPoints.size();

        cv::imshow("frame", channels[0]);
        cv::imshow("result", result);

        render();
        glfwSwapBuffers(win);
        glfwPollEvents();
    }
    return 0;
}

